//
//  GameScene.swift
//  SpriteKitIntro
//
//  Created by MacStudent on 2019-02-06.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {

    // Example 1  - Adding text to the screen
    let label = SKLabelNode(text:"HELLO WORLD")
    let label2 = SKLabelNode(text:"BYE!!!")
    
    
    // Example 2 - Draw a square on the screen
    let square = SKSpriteNode(color: SKColor.blue, size: CGSize(width: 50, height: 50))
    
    // Example 3 - Draw an image on the screen
    let duck = SKSpriteNode(imageNamed: "psyduck")
    
    // Example 4 - Draw a circle on the screen
    let circle = SKShapeNode(circleOfRadius: 10)
    
    
    
    
    override func update(_ currentTime: TimeInterval)
    {
        print("time : \(currentTime)")
    }
    
    
    /*
    //finger down
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        <#code#>
    }
 */
    
    
    //finger up
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches
        {
            let location = touch.location(in: self)
            let node = atPoint(location)
            
            // To get the touched half of the screen I do this
            if (location.x < self.size.width/2) {
                // left half touched, do something
                print("touched left")
            }
            
            if location.x >= self.size.width/2 {
                // right half touched, do something
                print("touched right")
            }

        }
    }
    override func didMove(to view: SKView) {
        // output the size of the screen
        print("Screen size (w,h): \(size.width)PX,\(size.height)PX")
        
        // Add images to the scene
        let bug = SKSpriteNode(imageNamed: "caterpie")
        bug.position = CGPoint(x:size.width/2, y:size.height/2)
        addChild(bug)
        
        duck.position = CGPoint(x:size.width/2+100, y:size.height/2)
        addChild(duck)
        
        
        // configure your text
        label.position = CGPoint(x:size.width/2, y:size.height/2)
        label.fontSize = 45
        label.fontColor = SKColor.yellow

        label2.fontSize = 60
        label2.position = CGPoint(x:size.width/2, y:200)
        
        // add it to your scene (draw it!)
        addChild(label)
        addChild(label2)
        
        // configure the square
        square.position = CGPoint(x: 105, y:700);
        // add square to scene
        addChild(square)
        
        // configure your circle
        // -----------------------
        // color of border
        circle.strokeColor = SKColor.red
        // width of border
        circle.lineWidth = 10
        // fill color
        circle.fillColor = SKColor.blue
        // location of circle
        circle.position = CGPoint(x:size.width/2, y:size.height/2)
        addChild(circle)
        
        //MOVEMENTS----
        //SINGLE MOVEMENTS
        
        //1----DEFINE A MOVEMENT
        let moveAction  = SKAction.moveBy(x: 50, y: 0, duration: 2)
        //in moveby x = 50 means he is gonna move every time 50 pixels in 2 seconds (distance)
        let moveAction4  = SKAction.moveBy(x: 0, y: 50, duration: 2)
        
        //let moveAction5  = SKAction.moveBy(x: 0, y: -50, duration: 2)
        
        
        
        
        //TO MOVE THINGS IN PATTERN
         let sequence:SKAction = SKAction.sequence([moveAction, moveAction4])
         //circle.run(sequence)
         //duck.run(sequence)
        
        
        //to move in reverse direction
        //duck.run(sequence.reversed())
        
        
        
        /*
         let moveAction1  = SKAction.moveTo(x: 300, duration: 4)
        //IN MOVE TO MEANS ---- MOVE THE X TO 300 COORDINATE (specific coordinate)
        
        
        let newPostion = CGPoint(x: 100, y: 700)
        let moveAction2  = SKAction.move(to: newPostion, duration: 4)
        
        //2----APLY MOVEMENT TO THE CHARACTER
        circle.run(moveAction)
        circle.run(moveAction4)
        */
        
        
        let sequence2 = SKAction.sequence([moveAction, moveAction4])
        //repeat forever
        //square.run(SKAction.repeatForever(sequence2))
        
        
        //repeat for certain numberS
        //circle.run(SKAction.repeat(sequence2, count: 2))
        
        let squarepostion2 = CGPoint(x: size.width/2, y: size.height/2)
        let moveToCircleAction = SKAction.move(to: squarepostion2, duration: 5)
        
        
        
        
        //if circle touches the square
        if(circle.frame.intersects(square.frame))
        {
            print("collosion detected")
        }
    }
    
}
